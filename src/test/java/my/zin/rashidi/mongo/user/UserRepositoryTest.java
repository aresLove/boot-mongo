package my.zin.rashidi.mongo.user;

import my.zin.rashidi.mongo.ApplicationTests;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * @author Rashidi Zin
 * @since 1.0.0-SNAPSHOT
 */
public class UserRepositoryTest extends ApplicationTests {

    @Autowired UserRepository $;

    private String id;

    @Before
    public void init() {
        id = $.save(
                new User("Tony Stark", "ironman")
        ).getId();
    }

    @Test
    public void findAll() {
        assertFalse(
                $.findAll()
                        .isEmpty()
        );
    }

    @Test
    public void findOneByUsername() {
        assertNotNull(
                $.findOneByUsername("ironman")
        );
    }

    @Test
    public void findAllByNameLikeIgnoringCase() {
        assertFalse(
                $.findAllByNameLikeIgnoringCase("tony")
                        .isEmpty()
        );
    }

    @After
    public void after() {
        $.delete(id);
    }
}
